// Values no matter of their type can be saved in variables for later use

// This is done with a assignment operator =
name = "Gordon";
age = 32;
isPrick = true;
description = `A "chef" with a very bloted ego.
Also likes to appear on reality TV.`;

// Variable names must only contain basic unicode characters and they cannot have spaces inside them
thisisaverylongvariablename = "And this is its contents";
// this would result in error = "Even if the contents are correct";

// To move around this problem, different solutions have been devised
// In JavaScript a system called camelCase is often use, in it each new word after first begins with a capital letter and articles are usually omitted
thisIsVeryLongVariableName = "That is now written with camelCase";

// Variable will retain its value until its replace with another value
name = "Lassie";
isDog = true;
console.log(name, isDog);
name = "Musti";
console.log(name, isDog);
name = "Miuku";
isDog = false;
console.log(name, isDog);

// Variables can be combined with certain keywords that affect them in certain ways

// Keyword const means that once the variable is assigned it can never be changed
const ageOfEarth = 5000000000;

// Trying to change a constant will result in an error
// ageOfEarth = 5000;