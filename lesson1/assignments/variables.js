/**
 * 1. Assign different values to a same variable while printing out its contents between every assingment
 * 
 * @Tip This is what we did last time so you should know...
 */




/**
 * 2. Write the following variable assignment using camelCase
 * lassieisaverygooddogthathaditsownmovieseries = true;
 * 
 * @Tip Remember that the first word is not capitalized
 */


/**
 * @Challenge
 * Since all unicode characters are valid text emojies may be used in text values
 * do the following variable assignmnent replacing as many words from the value with emojies as you can
 * and then print out that variable
 * 
 * name = "A black dog walked in a city and noticed a girl standing next to a policewoman.";
 * @Tip You can summon the emoji picker by pressing ctrl+cmd+space
 */