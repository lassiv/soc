 /**
  * 1. Print out both boolean values
  * 
  * @Tip Remember to end each command with a semicolon ;
  */




/**
 * 2. Write code that prints out different numbers, including decimals
 * 3. What is the largest number that you can print out, what about smallest, or the one closest to 0
 * 
 * @Tip Printing out can be done using console.log(12311);
 */




 /**
 * 4. Print out your own name and the names of 2 people sitting closest to you
 * 5. Write a short poem that spans multiple lines containing some empty lines and print it out
 * 
 * @Tip You can print out multiple things by separating them with commas console.log("first", "second", "third");
 */




 /**
  * @Challenge
  * Print out the poem that you wrote in 5 using single line strings, including all the empty strings
  * 
  * @Tip It's ok to use multiple console.log(); statements
  */