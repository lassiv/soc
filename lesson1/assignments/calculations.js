/**
 * 1. Solve the following math problems using code and print out the answers
 * a) 3 + 2 * 23
 * b) 100 / 100 * 100 * 0
 * c) 3 ^ 7
 * 
 * @Tip It's as simple as it sounds
 */


 


 /**
  * 2. Set 2 variables and use those variables in a sentence along with their sum
  * 
  * @Tip A got a dog that was 3 years old when i had it, its been 7 years since so now its 10 years old
  */





/**
 * 3. Print following number 1001001 using
 * a) 2 strings
 * b) a number, boolean and a string
 * c) 2 numbers and 8 strings
 * d) 1 number and boolean(s)
 * 
 * @Tip You can use () to alter the processing order
 */





/**
 * @Challenge
 * Write a code to solve a following problem and print out the anwser
 * 
 * If Emeline and Sam are stuck in an eternal loop of pranking,
 * each prank increases next pranks intensity by 0.17 and when the intensity
 * goes over 62 the next prank will result ending life as we know it.
 * How many months will it take for the world to end assuming
 * a) 3 pranks per month
 * b) 4 panks every other month and 7 every other
 * c) if amount of pranks increases by 1 every month, starting from 1 prank in the first month
 * Prank intensity starts at 0
 * 
 * @Tip Remeber that you can save intermediate values in variables
 */