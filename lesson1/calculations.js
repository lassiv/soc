// Calculations and combinations can be done with values (and variables)

// Regular operations with numbers function as they should
console.log("\nBasic");
console.log(3 + 5);
console.log(3 * 5);
console.log(3 - 5);
console.log(3 / 5);

// They can also be combined as usual
console.log("\nCombining");
console.log(3 * 3 * 3 - 3);

// Arithmetic rules are mostly respected so ordering is as in basic math
console.log("\nOrdering");
console.log(5 + 3 * 5);
console.log( (5 + 3) * 5);

// Strings cannot be combined numbers, and math can only be done with numbers
// Insted + plus sign is a combination operator with strings, it glues them together
console.log("\nStrings");
console.log("5" + "3");
console.log("5" + 3);
console.log("five" + "three");

// Booleans are a special case, they have a numerical value, true being 1 and false being 0
console.log("\nBooleans");
console.log(true + 1);
console.log(12314 * false);

// But as other "numbers" they cannot be calculated togerher with strings
console.log("\nBooleans and strings");
console.log(true + "three");
console.log(3 + "five" + true);

// If you want to combine results of calculations to strings you need to wrap the calculations in () parenthesis
console.log("\Calculations in strings");
console.log("The result of 3 * 5 * 28 is " + (3 * 5 * 28));

// Every operation can be done with both "raw values" and values in variables
// Following operations are exactly the same
let age = 53;
let incomePerYear = 40000;
let livingExpenseFactor = 0.6;
console.log("\nVariables in calculations");
console.log(53 * 40000 * 0.6);
console.log(age * incomePerYear * livingExpenseFactor);
console.log(age * 40000 * livingExpenseFactor);

// Intermediate steps can also be saved to their own values to make things more descriptive
let netIncome = incomePerYear * livingExpenseFactor;
console.log(age * netIncome);