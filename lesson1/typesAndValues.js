// There are different kind of types and values in JavaScript, here we introduce the "primitive" types

// Numbers can be written as they appear but without any spaces
1;
12415;
12684545;

// Decimals can be used with a decimal dot (.)
1.4;
12311.23421;
0.0032;


// Text must be wrapped in double-quotes "", this type is called string
"This is a sentence.";
"This is a bit longer sentence that goes on and on.";

// If you want to write something really long that spans mutiple lines you must wrap it in accent characters ``
`
This is a text that
spans multiple lines
and can be formatted at will.

Even empty lines can be added.


As many as you wish.
`;

// To indicate if something simply is or is not, a type called boolean is used. It has only 2 possible values
true;
false;
