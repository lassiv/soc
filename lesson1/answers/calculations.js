/**
 * 1. Solve the following math problems using code and print out the answers
 * a) 3 + 2 * 23
 * b) 100 / 100 * 100 * 0
 * c) 3 ^ 7
 * 
 * @Tip It's as simple as it sounds
 */
console.log(3 + 2 * 23);
console.log(100 / 100 * 100 * 0);
console.log(3 * 3 * 3 * 3 * 3 * 3 * 3);
// Alternative solutions
console.log(Math.pow(3,7));

 /**
  * 2. Set 2 variables and use those variables in a sentence along with their sum
  * 
  * @Tip I got a dog that was 3 years old when i had it, its been 7 years since so now its 10 years old
  */

let ageThen = 3;
let ageNow = 7;

console.log("I got a dog that was " + ageThen + " years old when i had it, its been " + ageNow + " years since so now its " + (ageThen + ageNow) + " years old");


/**
 * 3. Print following number 1001001 using
 * a) 2 strings
 * b) a number, boolean and a string
 * c) 2 numbers and 8 strings
 * d) 1 number and boolean(s)
 * 
 * @Tip You can use () to alter the processing order
 */

// Multiple ways to to do this
console.log("1001" + "001");
console.log(1 + false + "001001" );
console.log(1000000 + 1001 + "" + "" + "" + "" + "" + "" + "" + "");
console.log(1001000 + true);

/**
 * @Challenge
 * Write a code to solve a following problem and print out the anwser
 * 
 * If Emeline and Sam are stuck in an eternal loop of pranking,
 * each prank increases next pranks intensity by 0.17 and when the intensity
 * goes over 62 the next prank will result ending life as we know it.
 * How many months will it take for the world to end assuming
 * a) 3 pranks per month
 * b) 4 panks every other month and 7 every other
 * c) if amount of pranks increases by 1 every month, starting from 1 prank in the first month
 * Prank intensity starts at 0
 * 
 * @Tip Remeber that you can save intermediate values in variables
 */

 // a) 10 months
let count = 0;
count = count + 0 * 0.17 + 1 * 0.17 + 2 * 0.17;
count = count + 3 * 0.17 + 4 * 0.17 + 5 * 0.17;
count = count +  6 * 0.17 + 7 * 0.17 + 8 * 0.17;
count = count +  9 * 0.17 + 10 * 0.17 + 11 * 0.17;
count = count +  12 * 0.17 + 13 * 0.17 + 14 * 0.17;
count = count +  15 * 0.17 + 16 * 0.17 + 17 * 0.17;
count = count +  18 * 0.17 + 19 * 0.17 + 20 * 0.17;
count = count +  21 * 0.17 + 22 * 0.17 + 23 * 0.17;
count = count +  24 * 0.17 + 25 * 0.17 + 26 * 0.17;
console.log(count);
count = count +  27 * 0.17 + 28 * 0.17 + 29 * 0.17;
console.log(count);

// b) 6 months
count = 0;
count = count + 0 * 0.17 + 1 * 0.17 + 2 * 0.17 + 3 * 0.17;
count = count + 4 * 0.17 + 5 * 0.17 + 6 * 0.17 + 7 * 0.17 + 8 * 0.17 + 9 * 0.17 + 10 * 0.17;
count = count + 11 * 0.17 + 12 * 0.17 + 13 * 0.17 + 14 * 0.17;
count = count + 15 * 0.17 + 16 * 0.17 + 17 * 0.17 + 18 * 0.17 + 19 * 0.17 + 20 * 0.17 + 21 * 0.17;
count = count + 22 * 0.17 + 23 * 0.17 + 24 * 0.17 + 25 * 0.17;
console.log(count);
count = count + 26 * 0.17 + 27 * 0.17 + 28 * 0.17 + 29 * 0.17 + 30 * 0.17 + 31 * 0.17 + 32 * 0.17;
console.log(count);

// c) 7 months
count = 0;
count = count + 0 * 0.17;
count = count + 1 * 0.17 + 2 * 0.17;
count = count + 3 * 0.17 + 4 * 0.17 + 5 * 0.17;
count = count + 6 * 0.17 + 7 * 0.17 + 8 * 0.17 + 9 * 0.17;
count = count + 10 * 0.17 + 11 * 0.17 + 12 * 0.17 + 13 * 0.17 + 14 * 0.17;
count = count + 15 * 0.17 + 16 * 0.17 + 17 * 0.17 + 18 * 0.17 + 19 * 0.17 + 20 * 0.17;
console.log(count);
count = count + 21 * 0.17 + 22 * 0.17 + 23 * 0.17 + 24 * 0.17 + 25 * 0.17 + 26 * 0.17 + 27 * 0.17;
console.log(count);