 /**
  * 1. Print out both boolean values
  * 
  * @Tip Remember to end each command with a semicolon ;
  */


console.log(true, false);

/**
 * 2. Write code that prints out different numbers, including decimals
 * 3. What is the largest number that you can print out, what about smallest, or the one closest to 0
 * 
 * @Tip Printing out can be done using console.log(12311);
 */

console.log(2,1000,1.25,0.0248);
console.log(1.7976931348623157e+308, -1.7976931348623157e+308, 5e-324);

 /**
 * 4. Print out your own name and the names of 2 people sitting closest to you
 * 5. Write a short poem that spans multiple lines containing some empty lines and print it out
 * 
 * @Tip You can print out multiple things by separating them with commas console.log("first", "second", "third");
 */
console.log("Lassi", "Heikki", "Mika");
console.log(`
Three Rings for the Elven-kings under the sky,
Seven for the Dwarf-lords in their halls of stone,
Nine for Mortal Men doomed to die,

One for the Dark Lord on his dark throne
In the Land of Mordor where the Shadows lie.

One Ring to rule them all, One Ring to find them,
One Ring to bring them all, and in the darkness bind them,
In the Land of Mordor where the Shadows lie. 
`);


 /**
  * @Challenge
  * Print out the poem that you wrote in 5 using single line strings, including all the empty strings
  * 
  * @Tip It's ok to use multiple console.log(); statements
  */

  console.log("Three Rings for the Elven-kings under the sky,");
  console.log("Seven for the Dwarf-lords in their halls of stone,");
  console.log("Nine for Mortal Men doomed to die,");
  console.log("");
  console.log("One for the Dark Lord on his dark throne");
  console.log("In the Land of Mordor where the Shadows lie.");
  console.log("");
  console.log("One Ring to rule them all, One Ring to find them,");
  console.log("One Ring to bring them all, and in the darkness bind them,");
  console.log("In the Land of Mordor where the Shadows lie. ");