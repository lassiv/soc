/**
 * In programming there is often need to do same things over and over again
 * rather than copy-pasting same lines every single time such need arises
 * its much more convenient to create a re-usable function that can be called
 * when need arises.
 * 
 * Functions can be defined with (for example) following syntax
 */

 sayCongrats = () => {
    console.log("Congratulations.");
 };

/**
 * After a function has been defined the code inside the function can be executed
 * multiple times by calling the function with following syntax
 */

sayCongrats();

/**
* This can then be repeated as many times as necessary
*/

sayCongrats();
sayCongrats();
sayCongrats();

/**
 * There is no limit to how much code a function can contain, everything that is in the
 * function block (inside the {}-characters) is executed every time the function is.
 * Notice that since the function contains a random, its output will vary each time its
 * called;
 */

generateJohnWithAge = () => {
let age = Math.floor(Math.random() * 30) + 10;
console.log("Hi I'm John and I'm " + age + " years old.");
};

generateJohnWithAge();
generateJohnWithAge();
generateJohnWithAge();

/**
 * As with anything, functions can be nested and functions can call other functions.
 * They may contain all code structures such as if-clauses
 */

functionWithIfClauseAndFunctionCall = () => {
   let which = Math.floor(Math.random() * 2);
   console.log("Which was " + which);
   if(which < 1) {
      sayCongrats();
   } else {
      generateJohnWithAge();
   }
}

functionWithIfClauseAndFunctionCall();
functionWithIfClauseAndFunctionCall();