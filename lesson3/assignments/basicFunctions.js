/**
 * 1.   Create 3 functions that each print out a single sentence, then call those functions
 *      to create some awesome poetry. You can add some empty lines every now and then for
 *      extra style
 * @Tip Check lesson1 for printing out empty lines
 */




/**
 * 2.   Create a function that throws a 6-sided dice and prints out the result of the roll.
 *      Create similar functions for 10-sided and 20-sided dice as well. Test your functions.
 *      
 * @Tip Check lesson2 for random number generation
 */


/**
 *      @Challenge
 *      Write a function that generates a random number, prints it out, and if the number meets 
 *      certain criteria, generates another one by calling itself. This is done exactly
 *      the same way you would call any other function. You can decided on the random number
 *      range and criteria yourself.
 */