// In programming we have control clauses that are used to control how the execution of the program is done

// One of these is the if-clause that is used to execute code that matches certain conditions
// Its syntax is the following
if(true) {
    console.log("Im here");
}

// Where the first part if(true) defines a condition under which the code in the followup block should be executed
// 2nd part contains all the code that should be executed if condition is met { console.log("Im here"); }

// First part can contain any kind of comparison, for example
// Numerical
if(3 > 1);
// Equality, not that equality is denoted with triple ===, since single = is used for variable assigment
if("Robin" === "Robin");
// Or just purely a boolean
if(true);

// Below are all the possible numerical comparisons, all of these compare as true
// Greater than
if(3 > 1);
// Smaller than
if(1 < 3);
// Equal to
if(3 === 3);
// Greater than or equal to
if(3 >= 1);
// Smaller than or equal to
if(1 <= 3);

// Only equality can be used with strings

// Condition is the followed by a code block, eg. code that is between {}-chars. That will only be executed if condition is true
if(1 > 3) {
    console.log("One is greater than three");
}

if(3 > 1) {
    console.log("Three is greater than one");
}

// Blocks can contain as many lines of code as you want
if(4 > 2) {
    console.log("Brilliant");
    console.log("Four + Two = " + (4 + 2));
}

// They can even contain nested if-clauses
if(5 > 3) {
    console.log("Five is greater than three");
    if(2 < 1) {
        console.log("Two is smaller than one");
    }

    if(1 < 2) {
        console.log("One is smaller than two");
    }
}

// And again, all the values here can be replaced with variables
let small = 1;
let large = 5;
let text = small + " is smaller than " + large;
if(small < large) {
    console.log(text);
}