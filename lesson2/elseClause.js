// If-clause can be followed by an else clause where the code is executed if the if-clause was not true
let number = Math.floor(Math.random() * 4);
if(number > 2) {
    console.log("Number " + number + " is larger than 2");
} else {
    console.log("Number " + number + " is smaller or equal to 2");
}

// Syntax is if-clause, code block, else-clause and code block

// Each if-clause can only have a single else clause assigned to it, however nested clauses can each have their own elses
let anotherNumber = Math.floor(Math.random() * 10);
if(anotherNumber % 2 == 0) {
    console.log("Number " + anotherNumber + " is even.");
    if(anotherNumber > 3) {
        console.log("and larger than 3");
    } else {
        console.log("and smaller than 3");
    }
} else {
    console.log("Number " + anotherNumber + " is odd.")
}

// Else is executed only and only if the code in if-block was not executed, they are never ever executed in the same run