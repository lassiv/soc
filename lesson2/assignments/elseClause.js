/**
 *  1. Assingn a numercial value to a variable and then randomise a minumum score required for victory, then print out the number
 *  if it won and what was the score required to win
 * 
 * @Tip Create an else clause to process the not-winning scenario
 */

 


/**
 *  2. Assign a name to some variable, then compare that variable to some other variable containting the best name
 *  if the names match, print out "You have the best name", if not print out a witty remark about inadequate name
 */




 /**
  * @Challenge
  * Randomise an age, then randomise a life expectancy between 40 and 80 years. Compare the age to expectancy and if the person is alive
  * print out how old they are and how many years they still have left. If they are dead, print out how many years ago they died and at
  * what age.
  * 
  * @Tip Randomising something between 40 and 80 is actually no different from 0 and 40, but you need to somehow make the range start form 40
  */