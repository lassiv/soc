/**
 * 1.   Assign 2 names and ages to variables, compare the ages and print out which person is younger, change the ages to make sure that your if clause works
 * 
 * @Tip In total you will have 4 variables
 */



 /**
  * 2.  Create a clause structure where you check if variable age is larger than 16 and then print out the text "entry granted"
  *     if age is larger than 18 also print out the text "full access". Change the value of the age to check out that your if works as intended
  * 
  * @Tip Remember that you can have an if-clause inside another if-clause
  */



/**
 * 3.   Assing a winner number to variable winner with let winner = Math.floor(Math.random() * 30); print out the prize for victory from the following table
 *      0-10 Signed picture of Mr. Hansson
 *      11-20 Zervant T-Shirt
 *      21-30 Free entrance to Sokos Hotel Vantaa Tulisuudelma
 * 
 *  @Tip You need to nest your if-clauses to catch all conditions
 */

 /**
  *     @Challenge
  *     Math.floor(Math.random() * n); generates a random number between 0 and n - 1
  *     Crete a script that creates a random number between 0-5 and then creates 3 prizes and randomly assigns them to 3 numbers in then range
  *     script will then throw a dice (random 0-5) and tell user which prizes the user has won if any
  *     
  * @Tip Its ok if multiple prizes get assigned to the same number
  */