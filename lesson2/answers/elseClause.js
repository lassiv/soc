/**
 *  1. Assingn a numercial value to a variable and then randomise a minumum score required for victory, then print out the number
 *  if it won and what was the score required to win
 * 
 * @Tip Create an else clause to process the not-winning scenario
 */

let myNumber = 12;
let min = Math.floor(Math.random() * 20);
console.log("You score was " + myNumber + " and the score to beat was " + min);
if(myNumber > min) {
    console.log("You won");
} else {
    console.log("Better luck next time");
}


/**
 *  2. Assign a name to some variable, then compare that variable to some other variable containting the best name
 *  if the names match, print out "You have the best name", if not print out a witty remark about inadequate name
 */

let name = "Peppi";
if(name === "Peppi") {
    console.log("You have the best name");
} else {
    console.log("a witty remark about inadequate name");
}


 /**
  * @Challenge
  * Randomise an age, then randomise a life expectancy between 40 and 80 years. Compare the age to expectancy and if the person is alive
  * print out how old they are and how many years they still have left. If they are dead, print out how many years ago they died and at
  * what age.
  * 
  * @Tip Randomising something between 40 and 80 is actually no different from 0 and 40, but you need to somehow make the range start form 40
  */

  let age = Math.floor(Math.random() * 82);
  let expectancy = Math.floor(Math.random() * 41) + 40;

  if(age < expectancy) {
      console.log("You are " + age + " years old and still have " + (expectancy - age) + " years left!");
  } else {
      console.log("You died " + (age - expectancy) + " years ago at the age of " + expectancy);
  }