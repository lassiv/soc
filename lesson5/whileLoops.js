/**
 * We have previously taken a look at the for loop, and now will introduce another loop
 * the while-loop
 */

/**
 * Syntax is the keyword while, followed by a condition in a similar fashion as in if-loop
 * loop is then repeated as long as the condition is true. This will come handy with
 * certain HTML-related operations.
 */
while(true) {
    console.log('Just once');

    /**
     * We have a magic keyword break that can be used to immediately stop any
     * loop (that includes for-loop as well) and continue to whatever code
     * follows it
     */
    break;
}

/**
 * So in order to create a loop that prints out the numbers from 1 to 10 we
 * would create the following loop
 */

let number = 1;
while(number < 11) {
    console.log(number);
    /**
     * Notice that now we must remember to manually increment or alter whatever
     * value we use in the condition. If we don't remember to do that the loop will
     * run forever.
     */
    number++;
}

/**
 * Since the condition is true or false, we don't need to make a numerical comparison
 * but can use booleans as well if that suits us.
 */

let hasntWonAnything = true;
while(hasntWonAnything) {
    if(Math.random() * 10 > 6) {
        console.log('You won!');
        /**
         * This is now the condition variable so we must remember to alter its value
         * or the loop will just run forever
         */
        hasntWonAnything = false;
    } else {
        console.log('You did not win');
    }
}