/**
 * Programming is useful for automating repetitive tasks. For that reason
 * we have different kinds of loops that well repeat the same code over
 * and over again. Here we are going to give a simple introduciton to 
 * for-loop.
 */

/**
 * For-loop runs the same code block as many times as is stated in the 
 * condition part. Syntax is the following: Introduce a variable to increment
 * check if the variable is withing the condition and increment the variable
 */

for(let i = 0; i < 10; i++) {
    console.log("This will be printed 10 times", i);
};

/**
 * As you can see the variable i is incremented every single time loop is ran
 * as long as it matches the condition eg. is smaller than 10. Once condition is
 * reached repetition is stopped.
 */