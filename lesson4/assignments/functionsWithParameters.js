/**
 * 1.   Create a function that tells a short story and allows the user to alter
 *      the story by giving some amount of parameters to the function. Test your
 *      function with multiple different parameters.
 * 
 * @Tip Probably a good idea to use multiple console.log statements instead of one very long
 */



/**
 * 2.   Create a function that calculates and prints out the area of a right triangle when
 *      given the lengths of its short sides as parameters
 * 
 * @Tip Area of a right triangle is height times width divided by 2 ( T = (a * b) / 2 )
 */





 
/**
 * 3.   Create a function to calculate the average of 5 different numbers. Function must
 *      print out all the numbers and then their average.
 * 
 * @Tip Average is sum of terms divided by the number of terms
 */


 /**
  * @Warning    Difficulty level 9000
  * @Challenge  Since a function is just like any other value, it can be given as a parameter
  *             to a function and then called from that function. Write a function (X) that takes
  *             others functions (Ys) and a single number as a parameter. Function Ys must all accept 
  *             single numerical parameter and print out useful information based on that 
  *             (such as area of a square). Function X will then call given function Y with a number
  *             that ranges from -15 to the parameter given to function X. Try out your function
  *             with different parameters.
  * 
  * @Tip        let functionX = (upToThisNumber, callThisFunction) => {...};
  */