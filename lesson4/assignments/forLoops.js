/**
 * 1.   Print out numbers between 1 and 10000
 * 
 * @Tip Please use loop
 */




/**
 * 2.   Create a function "triangle" that will print out a triangle made of
 *      *-symbols. Function should take the number of rows as a parameter
 *      and the number of starts should start from 1 on a first row and then
 *      increase by one per row. Output with number 4 should look like:
 *      
 *      *
 *      **
 *      ***
 *      ****
 * 
 * @Tip You can add to a text variable with myText = myText + "*";
 */



 

/**
 * 3.   Create a function that will run forever
 * 
 * @Tip You can stop the code from running by secondary clicking the output and
 *      choosing "Stop Code Run"
 */




 /**
  * @Challenge  Create a general version of the dice throwing function that takes
  *             two parameters, number of dice to throw and number of sides in
  *             each die. Then the function will throw given number of dice and
  *             print out each individual throw and combined total of all throws.
  * 
  * @Tip        Remember condition in for-loop can be a comparison between 2
  *             variables.
  */