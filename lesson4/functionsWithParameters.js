/**
 * In the last part we took a look at functions, and they while useful
 * were missing some key elements. This time we are going to take a look
 * at parameters and how to use those with functions
 */

/**
 * We have a function that is used to print out a sentence that contains
 * a greeting and the name of the person being greeted. Based on what we
 * learned before, we would write something like this.
 */

let sayHelloToJohn = () => {
    console.log(`Hello John, its a very fine day today.`);
};

/**
 * This way we can replicate the functionality simply by calling the function
 */

sayHelloToJohn();
sayHelloToJohn();

/**
 * A problem arises when we want to say hello to someone who is not John.
 * What do we do then? Do we just write a new function to every single name we
 * need to print? Surely there must be a better way? Enter parameters.
 */

/**
 * A function can have any number of named parameters that function exactly the
 * same as parameters in a mathematical function. They are defined in following
 * manner.
 */

// We can choose any name for it, but descriptive ones are good, hence "name"
let sayHelloTo = (name) => {
    // Now we can refer to this parameter as it were any other variable
    // But only inside the block of this function
    console.log("Hello " + name + ", its a very fine day today.");
};

// Now we can use the same function to print the greeting with any name we provide
sayHelloTo("Jane");
sayHelloTo("Tarzan");

/**
 * Parameters can be of any possible type, anything you can assign to a variable
 * you can use as a parameter. Consider all dice rolling that we did last time,
 * you can use parameters to create a generic function to roll n-sided die.
 */

let rollDice = (numberOfSides) => {
    let result = Math.floor(Math.random() * numberOfSides) + 1;
    console.log("A die with " + numberOfSides + " sides was cast. Result was " + result);
};

rollDice(6);
rollDice(10);

/**
 * Since you can have any number of parameters you can create combinations
 */

let greeting = (name, maximumAge) => {
    let realAge = Math.floor(Math.random() * maximumAge);
    console.log("Nice to see you " + name + ". Did you know that you are " + realAge + " years old?");
};

greeting("Tarzan", 60);
greeting("Jane", 60);